public class uploadInventry {
    
    //get Inventory Transcation Master Record Status Picklist Values
    @AuraEnabled 
    public static Map<String, String> getRecordStatus(){
        Map<String, String> options = new Map<String, String>();
        //get Account Industry Field Describe
        Schema.DescribeFieldResult fieldResult = Inventory_Transaction_Master__c.Record_Type__c.getDescribe();
        //get Account Industry Picklist Values
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            //Put Picklist Value & Label in Map
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
    //get Inventory Transcation Master Batch Status Picklist Values
    @AuraEnabled 
    public static Map<String, String> getBatchStatus(){
        Map<String, String> options = new Map<String, String>();
        //get Account Industry Field Describe
        Schema.DescribeFieldResult fieldResult = Inventory_Transaction_Master__c.Batch_Status__c.getDescribe();
        //get Account Industry Picklist Values
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            //Put Picklist Value & Label in Map
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
    @AuraEnabled 
    public static Map<String, String> getMainettiMap(){
        getMasterData mainettiMap = new getMasterData();
        return mainettiMap.getManitteCompanyPicklistMap();
    }
    
    @AuraEnabled
    public static List<Inventory_Transaction_Master__c> getDateandTypeRecords(date fromDate, date toDate, string recordStatus, string batchStatus, string mainettiCmp){
        string searchQuery = 'select Id, Batch_No__c, Batch_Status__c, File_Uploaded_By__c, Record_Type__c,Azure_File_Link__c,Mainetti_Company__c from Inventory_Transaction_Master__c';
        if(!String.isBlank(string.valueOf(fromDate)) || !String.isBlank(string.valueOf(toDate))||!String.isBlank(recordStatus)||!String.isBlank(batchStatus)||!String.isBlank(mainettiCmp)){
            searchQuery += ' where ';
        }
        
        boolean dateFieldFrmPresent = false;
        boolean dateFieldToPresent = false;
        boolean recordStatusPresent = false;
        boolean batchStatusPresent = false;
        if(!String.isBlank(string.valueOf(fromDate)) || !String.isBlank(string.valueOf(toDate))){
            if(!String.isBlank(string.valueOf(fromDate)) && !String.isBlank(string.valueOf(toDate))){
                searchQuery += ' DAY_ONLY(CreatedDate) >=:fromDate AND DAY_ONLY(CreatedDate) <=:toDate';       
                dateFieldFrmPresent = true; 
                dateFieldToPresent = true;
            }
            if(!String.isBlank(string.valueOf(fromDate)) && String.isBlank(string.valueOf(toDate))){
                searchQuery += ' DAY_ONLY(CreatedDate) >=:fromDate';
                dateFieldFrmPresent = true;
            }
            if(String.isBlank(string.valueOf(fromDate)) && !String.isBlank(string.valueOf(toDate))){
                searchQuery += ' DAY_ONLY(CreatedDate) >=:toDate';
                dateFieldToPresent = true;
            }            
        }
        if(!String.isBlank(recordStatus)){
            if(dateFieldFrmPresent==true || dateFieldToPresent==true){
            	searchQuery += ' AND Record_Type__c=:recordStatus';
            }else{
                searchQuery += ' Record_Type__c=:recordStatus';
            }
            recordStatusPresent = true;
        }
        if(!String.isBlank(batchStatus)){
            if(dateFieldFrmPresent==true || dateFieldToPresent==true || recordStatusPresent==true ){
                if(batchStatus == '1 - Migration Success'){
                    batchStatus = '8 - Migration Success';
                    searchQuery += ' AND Batch_Status__c=:batchStatus';
                }
                else{
                    batchStatus = '8 - Migration Success';
                    searchQuery += ' AND Batch_Status__c!=:batchStatus';
                }            	
            }else{                
                if(batchStatus == '1 - Migration Success'){
                    batchStatus = '8 - Migration Success';
                    searchQuery += ' Batch_Status__c=:batchStatus';
                }
                else{
                    batchStatus = '8 - Migration Success';
                    searchQuery += ' Batch_Status__c!=:batchStatus';
                } 
            }
            batchStatusPresent = true;
        }
        if(!String.isBlank(mainettiCmp)){
            //system.debug('querymainettiCmp:'+mainettiCmp);
            if(dateFieldFrmPresent==true || dateFieldToPresent==true || recordStatusPresent==true || batchStatusPresent==true){
            	searchQuery += ' AND Mainetti_Company__c=:mainettiCmp';
            }else{
                searchQuery += ' Mainetti_Company__c=:mainettiCmp';
            }
        }
        searchQuery += ' order by CreatedDate';
        //system.debug('query:'+searchQuery);
        List<Inventory_Transaction_Master__c> fetchdata= Database.query(searchQuery);
	    //system.debug('fetchdata:'+fetchdata.size());        
        return fetchdata;
    }
    
    
    //Get data of Inventory_Transaction_Master__c object.
    @AuraEnabled
    public static List<Inventory_Transaction_Master__c> getData(){
        List<Inventory_Transaction_Master__c> fetchdata=[select Id, Batch_No__c, Batch_Status__c, File_Uploaded_By__c, Record_Type__c,Azure_File_Link__c,Mainetti_Company__c,Total_Row_Count__c,Uploaded_Total_Stock_In_Qty__c from Inventory_Transaction_Master__c order by CreatedDate desc];
        return fetchdata;
    }
    
    //Get error details of Inventory_Transaction_Stage__c.
    @AuraEnabled
    public static List<Inventory_Transaction_Stage__c> getErrorData(string batchid){
        List<Inventory_Transaction_Stage__c> fetchErrordata=[Select Warehouse__c,Warehouse_FB__c,Warehouse_NF__c,Color__c,Color_FB__c,Color_NF__c,Inventory_Model__c,Inventory_Model_FB__c,Inventory_Model_NF__c,Name,Local_System_SKU__c,Mainetti_Company__c,MC_NF__c,Manitte_Company_FB__c,Price_Book_Spec_NF__c,
                                                             Record_Status__c,Remarks__c,Retailer_Code__c,Retailer_Code_FB__c,Retailer_Code_NF__c,Row_No__c,Sizer_Print__c,Sizer_Print_NF__c,Source__c,Source_FB__c,Stock_In_Date__c,Stock_In_Date_FB__c,Stock_In_Qty__c,Stock_In_Qty_FB__c,Upload_Batch_No__c from Inventory_Transaction_Stage__c where
                                                             Upload_Batch_No__c=:batchid];
        system.debug('fetchErrordata::'+fetchErrordata.size());
        return fetchErrordata;
    }
    
    public static List<Inventory_Transaction_Stage__c> getSuccessData(string batchid){
        List<Inventory_Transaction_Stage__c> fetchSuccessdata=[Select Warehouse__c,Warehouse_FB__c,Warehouse_NF__c,Color__c,Color_FB__c,Color_NF__c,Inventory_Model__c,Inventory_Model_FB__c,Inventory_Model_NF__c,Name,Local_System_SKU__c,Mainetti_Company__c,MC_NF__c,Manitte_Company_FB__c,Price_Book_Spec_NF__c,
                                                               Record_Status__c,Remarks__c,Retailer_Code__c,Retailer_Code_FB__c,Retailer_Code_NF__c,Row_No__c,Sizer_Print__c,Sizer_Print_NF__c,Source__c,Source_FB__c,Stock_In_Date__c,Stock_In_Date_FB__c,Stock_In_Qty__c,Stock_In_Qty_FB__c,Upload_Batch_No__c from Inventory_Transaction_Stage__c where
                                                               Upload_Batch_No__c=:batchid and Record_Status__c ='2 - Data Validation Successful'];
        return fetchSuccessdata;
    }
    
    @AuraEnabled
    public static boolean createDocument(string csv, string batchNo,String fileName) { 
        
        boolean createStatus = new getMasterData().createDocument(batchNo,csv,fileName);
        system.debug('createDocumentcreateStatus::'+createStatus);
        return createStatus;
    }
    
    //Inserting records into both the objects.
    @AuraEnabled
    public static String parseInventory(string jsonin, string batchNo, string recordType, string company) {  
        string returnBatchId = '';
        Inventory_Transaction_Master__c updateBatchStatus = new Inventory_Transaction_Master__c();
        boolean recordStatus = true;                
        if(!string.isBlank(batchNo)){
            List<Inventory_Transaction_Stage__c> fetchErrordata=[Select Id,Mainetti_Company__c from Inventory_Transaction_Stage__c where Upload_Batch_No__c=:batchNo];
              updateBatchStatus =[Select Id, Mainetti_Company__c from Inventory_Transaction_Master__c where Id=:batchNo];
            if(fetchErrordata.size() > 0)
            {                
                try{
                    delete fetchErrordata;
                    boolean status=insertInventoryStage(jsonin,batchNo);
                    if(!status){           
                        recordStatus = false;                        
                    }
                }catch(Exception e){   
                    recordStatus = false;
                    system.debug('Error in parse: deleting stage records:'+e.getMessage());
                }                
            }            
        }else{              
            if(recordStatus){
                try{
                    Inventory_Transaction_Master__c Transactions = new Inventory_Transaction_Master__c();
                        Transactions.File_Uploaded_By__c = userInfo.getFirstName() + ','+ userInfo.getLastName();
                        Transactions.Record_Type__c = recordType;
                    	Transactions.Mainetti_Company__c = company;
                        insert Transactions;
                        boolean status=insertInventoryStage(jsonin,Transactions.Id);
                        updateBatchStatus.Id=Transactions.Id;
                }catch(DMLException e){   
                    recordStatus = false;
                    system.debug('Error in parse : during batch creation:'+e.getMessage());
                }
            }            
        }
        if(recordStatus){
            try{
                returnBatchId = updateBatchStatus.Id;
                updateBatchStatus.Batch_Status__c='2 - Data Validation In Progress';  
                updateBatchStatus.Mainetti_Company__c = company;
                update updateBatchStatus;
                boolean validateStatus = new processInventoryData().validateUploadRows(updateBatchStatus);
                system.debug('validateStatus::::'+validateStatus);
                if(!validateStatus){
                    updateBatchStatus.Batch_Status__c='3 - Error In Data Validation';   
                    update updateBatchStatus;
                    List<Inventory_Transaction_Stage__c> transferRecords =  getErrorData(updateBatchStatus.Id);
                    //failure records are there,data validation validation failure.
                    if(transferRecords.size() > 0){
                        recordStatus = false;              
                    }
                }/*else{
                    recordStatus = false; 
                } */               
            }catch(DMLException e){   
                recordStatus = false;
                system.debug('Error in parse: during data validation:'+e.getMessage());
            }
        }   
        if(!recordStatus){
            returnBatchId = '';
        }
        return returnBatchId;
    }	
    public boolean insertDataUploadTransaction(Inventory_Transaction_Master__c updateBatchStatus){
        
        boolean insertStatus = true;
        getMasterData masterData = new getMasterData();
        processInventoryData processInvData = new processInventoryData();
        map<string,Id> retailerCodeWithId = masterData.getRetailerCodeIdMap();        
        map<string,Id> mainettiCompanyWithId = masterData.getManitteCompanyIdMap();       
        List<Inventory_Transaction_Stage__c> transferRecords =  getSuccessData(updateBatchStatus.Id);
        List<Data_Upload_Transaction__c> insertRecord = new List<Data_Upload_Transaction__c>();
        if(transferRecords.size() > 0){
            Map<string,string> invTransOrderToCompMap = masterData.getListOfTransManitteCompany(updateBatchStatus.Id);
            Map<string,Map<string,string>> modelPriceMap = masterData.getRTSModelMap(updateBatchStatus);
            Map<string,string> rtsPriceMap = modelPriceMap.get('Price');            
            updateBatchStatus.Batch_Status__c = '7 - Migration In Progress';
            update updateBatchStatus;
            
            for(Inventory_Transaction_Stage__c trans: transferRecords){
                Data_Upload_Transaction__c invTransMast = new Data_Upload_Transaction__c();
                invTransMast.Upload_Batch_No__c = trans.Upload_Batch_No__c;
                invTransMast.Record_Type__c = updateBatchStatus.Record_Type__c;
                invTransMast.Retailer_Code__c = retailerCodeWithId.get(trans.Retailer_Code__c);
                invTransMast.Mainetti_Company__c = mainettiCompanyWithId.get(trans.Mainetti_Company__c);
                string pricbookspec = '';
                if(trans.Retailer_Code__c == 'LOJAS RENNER'){
                    pricbookspec = trans.Inventory_Model__c+'#'+trans.Retailer_Code__c+'#'+trans.Mainetti_Company__c+'#'+trans.Color__c+'#'+trans.Sizer_Print__c;
                }else{
                    pricbookspec =  trans.Inventory_Model__c+'#'+trans.Retailer_Code__c+'#'+trans.Mainetti_Company__c+'#'+trans.Color__c;
                }               
                String inventoryModal =  rtsPriceMap.get(pricbookspec.toLowerCase()); 
                invTransMast.Inventory_Model__c = inventoryModal;
                invTransMast.Warehouse__c = trans.Warehouse__c;
                invTransMast.Color__c = trans.Color__c;
                invTransMast.Sizer_Print__c = trans.Sizer_Print__c;
                invTransMast.Local_System_SKU__c = trans.Local_System_SKU__c;
                invTransMast.Source__c = trans.Source__c;
                invTransMast.Stock_In_Date__c =processInvData.parseLongDate(trans.Stock_In_Date__c);
                invTransMast.Stock_In_Qty__c = Decimal.valueOf(trans.Stock_In_Qty__c);                
                insertRecord.add(invTransMast);
            }
            if(insertRecord.size()>0){
                try{
                    insert insertRecord;
                    delete transferRecords;
                }catch(DMLException e){   
                    insertStatus = false;
                    system.debug('Error in insertDataUploadTransaction: during DataUploadTransaction'+e.getMessage());
                }
            }            
        }
        return insertStatus;
    }
    
    public static boolean insertInventoryStage(string jsonin, string batchNo){
        boolean insertStatus = true;
        List<Inventory_Transaction_Stage__c> deserializedInventory = (List<Inventory_Transaction_Stage__c>)JSON.deserialize(jsonin, List<Inventory_Transaction_Stage__c>.class);
        List<Inventory_Transaction_Stage__c> transbatchid = new  List<Inventory_Transaction_Stage__c>();
        double rowNo = 1;
        /*Inventory_Transaction_Stage__c header = new Inventory_Transaction_Stage__c();
        header.Row_No__c=000;
        header.Retailer_Code__c='Retailer_Code';
        transbatchid.add(header);*/
        for(Inventory_Transaction_Stage__c trans: deserializedInventory){
            //if(String.isBlank(trans.Upload_Batch_No__c) && )
            trans.Row_No__c =rowNo;
            trans.Upload_Batch_No__c = batchNo;
            transbatchid.add(trans);
            rowNo++;
        }
        try{
            insert transbatchid;
        }catch(DMLException e){   
            system.debug('Error in insertInventoryStage: during DataUploadTransaction'+e.getMessage());
            insertStatus = false;
        }
        return insertStatus;      
    }
    public class applicationException extends Exception {}
    
    /*public static void deleteDataUploadTransaction(){
        list<Inventory_Transaction_Stage__c>results = [select Id from Inventory_Transaction_Stage__c ];
        delete results;
    }*/
    public  void insertTestToData(String batchId){
        Inventory_Transaction_Master__c invTransMaster = [Select Id, Batch_No__c, Batch_Status__c,Record_Type__c  from Inventory_Transaction_Master__c where Id =:batchId];
        if(invTransMaster != null){
            boolean status  = new uploadInventry().insertDataUploadTransaction(invTransMaster);
        }
    }
}